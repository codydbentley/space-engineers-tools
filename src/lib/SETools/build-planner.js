
import SpaceEngineers from "../SpaceEngineers";

export default {
    SearchBlocks: (query, gridType) => {
        let blocks = [];
        let keys = Object.keys(SpaceEngineers.Blocks);
        keys.forEach(key => {
            let block = SpaceEngineers.Blocks[key];
            if (block.name.toLowerCase().indexOf(query.toLocaleLowerCase()) > -1 && block.gridType === gridType) {
                blocks.push(block);
            }
        });
        return blocks;
    },
    GenerateComponentList: (blocks) => {
        let components = [];
        blocks.forEach(entry => {
            let required = SpaceEngineers.Services.Block.Requires(entry.block, entry.quantity);
            required.forEach(build => {
                let found = false;
                components.forEach(comp_entry => {
                    if (comp_entry.component === build.component) {
                        comp_entry.quantity += build.amount;
                        found = true;
                    }
                });
                if (!found) {
                    components.push({
                        component: build.component,
                        quantity: build.amount
                    });
                }
            });
        });
        return components;
    },
    GenerateMaterialList: (components) => {
        let materials = [];
        components.forEach(entry => {
            let required = SpaceEngineers.Services.Component.Requires(entry.component, entry.quantity);
            required.forEach(assembly => {
                let found = false;
                materials.forEach(material_entry => {
                    if (material_entry.material === assembly.material) {
                        material_entry.quantity += assembly.amount;
                        found = true;
                    }
                });
                if (!found) {
                    materials.push({
                        material: assembly.material,
                        quantity: assembly.amount
                    });
                }
            });
        });
        return materials;
    },
    GenerateOreList: (materials, modules) => {
        let ore = [];
        materials.forEach(entry => {
            let required = SpaceEngineers.Services.Material.Requires(entry.material, entry.quantity, modules);
            required.forEach(refined => {
                let found = false;
                ore.forEach(ore_entry => {
                    if (ore_entry.ore === refined.ore) {
                        ore_entry.quantity += refined.amount;
                        found = true;
                    }
                });
                if (!found) {
                    ore.push({
                        ore: refined.ore,
                        quantity: refined.amount
                    });
                }
            });
        });
        return ore;
    }
}