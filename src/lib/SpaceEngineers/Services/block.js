import Building from "../building";

const Block = {
    Requires: (block, amount) => {
        let components = [];
        Building.forEach(built => {
            if (built.block === block) {
                components.push({
                    component: built.component,
                    amount: amount * built.ratio
                });
            }
        });
        return components;
    },
    GetWeight: (block, amount) => {
        let total = 0;
        Block.Requires(block, amount).forEach(required => {
            total += required.amount * required.component.weight;
        });
        return total;
    }
};

export default Block;