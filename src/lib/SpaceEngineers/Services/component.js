import Assembling from "../assembling";
import Building from "../building";

export default {
    Requires: (component,  amount) => {
        let materials = [];
        Assembling.forEach(assembled => {
            if (assembled.component === component) {
                materials.push({
                    material: assembled.material,
                    amount: amount * assembled.ratio
                });
            }
        });
        return materials;
    },
    Build: (component, amount) => {
        let blocks = [];
        Building.forEach(built => {
            if (built.component === component) {
                blocks.push({
                    block: built.block,
                    amount: Math.floor(amount / built.ratio)
                });
            }
        });
        return blocks;
    }
};