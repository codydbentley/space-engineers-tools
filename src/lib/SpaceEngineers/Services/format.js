export default {
    Kilograms: (amt) => {
        return amt.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + ' kg';
    },
    Liters: (amt) => {
        return amt.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + ' L';
    }
};