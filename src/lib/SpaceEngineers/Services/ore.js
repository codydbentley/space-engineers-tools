import {Refining, YieldModifier} from "../refining";

export default {
    Refine: (ore, amount, modules) => {
        let materials = [];
        Refining.forEach(refining => {
            if (refining.ore === ore) {
                materials.push({
                    material: refining.material,
                    amount: amount * refining.ratio * YieldModifier[modules].modifier
                });
            }
        });
        return materials;
    }
};