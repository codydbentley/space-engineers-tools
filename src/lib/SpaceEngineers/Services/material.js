import {Refining, YieldModifier} from "../refining";
import Assembling from "../assembling";

export default {
    Requires: (material, amount, modules) => {
        let ores = [];
        Refining.forEach(refining => {
            if (refining.material === material) {
                ores.push({
                    ore: refining.ore,
                    amount: amount / refining.ratio / YieldModifier[modules].modifier
                });
            }
        });
        return ores;
    },
    Assemble: (material, amount) => {
        let components = [];
        Assembling.forEach(assembled => {
            if (assembled.material === material) {
                components.push({
                    component: assembled.component,
                    amount: Math.floor(amount / assembled.ratio)
                });
            }
        });
        return components;
    }
};