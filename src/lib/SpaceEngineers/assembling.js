import Components from "./components";
import Materials from "./materials";

export default [
    // Components
    {component: Components.BulletproofGlass, material: Materials.SiliconWafer, ratio: 5},

    {component: Components.Canvas, material: Materials.SiliconWafer, ratio: 11.67},
    {component: Components.Canvas, material: Materials.IronIngot, ratio: 0.67},

    {component: Components.Computer, material: Materials.IronIngot, ratio: 0.17},
    {component: Components.Computer, material: Materials.SiliconWafer, ratio: 0.07},

    {component: Components.ConstructionComponent, material: Materials.IronIngot, ratio: 2.67},

    {component: Components.Datapad, material: Materials.IronIngot, ratio: 0.33},
    {component: Components.Datapad, material: Materials.SiliconWafer, ratio: 1.67},
    {component: Components.Datapad, material: Materials.Gravel, ratio: 0.33},

    {component: Components.DetectorComponent, material: Materials.IronIngot, ratio: 1.67},
    {component: Components.DetectorComponent, material: Materials.NickelIngot, ratio: 5.00},

    {component: Components.Display, material: Materials.IronIngot, ratio: 0.33},
    {component: Components.Display, material: Materials.SiliconWafer, ratio: 1.67},

    {component: Components.Explosive, material: Materials.SiliconWafer, ratio: 0.17},
    {component: Components.Explosive, material: Materials.MagnesiumPowder, ratio: 0.67},

    {component: Components.Girder, material: Materials.IronIngot, ratio: 2.00},

    {component: Components.GravityComponent, material: Materials.SilverIngot, ratio: 1.67},
    {component: Components.GravityComponent, material: Materials.GoldIngot, ratio: 3.33},
    {component: Components.GravityComponent, material: Materials.CobaltIngot, ratio: 73.33},
    {component: Components.GravityComponent, material: Materials.IronIngot, ratio: 200.00},

    {component: Components.InteriorPlate, material: Materials.IronIngot, ratio: 1.00},

    {component: Components.LargeSteelTube, material: Materials.IronIngot, ratio: 10.00},

    {component: Components.MedicalComponent, material: Materials.IronIngot, ratio: 20.00},
    {component: Components.MedicalComponent, material: Materials.NickelIngot, ratio: 23.33},
    {component: Components.MedicalComponent, material: Materials.SilverIngot, ratio: 6.67},

    {component: Components.MetalGrid, material: Materials.IronIngot, ratio: 4.00},
    {component: Components.MetalGrid, material: Materials.NickelIngot, ratio: 1.67},
    {component: Components.MetalGrid, material: Materials.CobaltIngot, ratio: 1.00},

    {component: Components.MissileContainer200mm, material: Materials.IronIngot, ratio: 18.33},
    {component: Components.MissileContainer200mm, material: Materials.NickelIngot, ratio: 2.33},
    {component: Components.MissileContainer200mm, material: Materials.SiliconWafer, ratio: 0.07},
    {component: Components.MissileContainer200mm, material: Materials.UraniumIngot, ratio: 0.03},
    {component: Components.MissileContainer200mm, material: Materials.PlatinumIngot, ratio: 0.01},
    {component: Components.MissileContainer200mm, material: Materials.MagnesiumPowder, ratio: 0.40},

    {component: Components.Motor, material: Materials.IronIngot, ratio: 6.67},
    {component: Components.Motor, material: Materials.NickelIngot, ratio: 1.67},

    {component: Components.NATOAmmoContainer25x184mm, material: Materials.IronIngot, ratio: 13.33},
    {component: Components.NATOAmmoContainer25x184mm, material: Materials.NickelIngot, ratio: 1.67},
    {component: Components.NATOAmmoContainer25x184mm, material: Materials.MagnesiumPowder, ratio: 1.00},

    {component: Components.NATOAmmoMagazine5_56x45mm, material: Materials.IronIngot, ratio: 0.27},
    {component: Components.NATOAmmoMagazine5_56x45mm, material: Materials.NickelIngot, ratio: 0.07},
    {component: Components.NATOAmmoMagazine5_56x45mm, material: Materials.MagnesiumPowder, ratio: 0.02},

    {component: Components.PowerCell, material: Materials.IronIngot, ratio: 3.33},
    {component: Components.PowerCell, material: Materials.SiliconWafer, ratio: 0.33},
    {component: Components.PowerCell, material: Materials.NickelIngot, ratio: 0.67},

    {component: Components.RadioCommComponent, material: Materials.IronIngot, ratio: 2.67},
    {component: Components.RadioCommComponent, material: Materials.SiliconWafer, ratio: 0.33},

    {component: Components.ReactorComponent, material: Materials.IronIngot, ratio: 5.00},
    {component: Components.ReactorComponent, material: Materials.Gravel, ratio: 6.67},
    {component: Components.ReactorComponent, material: Materials.SilverIngot, ratio: 1.67},

    {component: Components.SmallSteelTube, material: Materials.IronIngot, ratio: 1.67},

    {component: Components.SolarCell, material: Materials.NickelIngot, ratio: 1.00},
    {component: Components.SolarCell, material: Materials.SiliconWafer, ratio: 2.00},

    {component: Components.SteelPlate, material: Materials.IronIngot, ratio: 7.00},

    {component: Components.Superconductor, material: Materials.IronIngot, ratio: 3.33},
    {component: Components.Superconductor, material: Materials.GoldIngot, ratio: 0.67},

    {component: Components.ThrusterComponent, material: Materials.IronIngot, ratio: 10.00},
    {component: Components.ThrusterComponent, material: Materials.CobaltIngot, ratio: 3.33},
    {component: Components.ThrusterComponent, material: Materials.GoldIngot, ratio: 0.33},
    {component: Components.ThrusterComponent, material: Materials.PlatinumIngot, ratio: 0.13},
    // Weapons and Tools
    {component: Components.Grinder, material: Materials.IronIngot, ratio: 1.00},
    {component: Components.Grinder, material: Materials.NickelIngot, ratio: 0.33},
    {component: Components.Grinder, material: Materials.Gravel, ratio: 1.67},
    {component: Components.Grinder, material: Materials.SiliconWafer, ratio: 0.33},

    {component: Components.EnhancedGrinder, material: Materials.IronIngot, ratio: 1.00},
    {component: Components.EnhancedGrinder, material: Materials.NickelIngot, ratio: 0.33},
    {component: Components.EnhancedGrinder, material: Materials.CobaltIngot, ratio: 0.67},
    {component: Components.EnhancedGrinder, material: Materials.SiliconWafer, ratio: 2.00},

    {component: Components.ProficientGrinder, material: Materials.IronIngot, ratio: 1.00},
    {component: Components.ProficientGrinder, material: Materials.NickelIngot, ratio: 0.33},
    {component: Components.ProficientGrinder, material: Materials.CobaltIngot, ratio: 0.33},
    {component: Components.ProficientGrinder, material: Materials.SiliconWafer, ratio: 0.67},
    {component: Components.ProficientGrinder, material: Materials.SilverIngot, ratio: 0.67},

    {component: Components.EliteGrinder, material: Materials.IronIngot, ratio: 1.00},
    {component: Components.EliteGrinder, material: Materials.NickelIngot, ratio: 0.33},
    {component: Components.EliteGrinder, material: Materials.CobaltIngot, ratio: 0.33},
    {component: Components.EliteGrinder, material: Materials.SiliconWafer, ratio: 0.67},
    {component: Components.EliteGrinder, material: Materials.PlatinumIngot, ratio: 0.67},

    {component: Components.AutomaticRifle, material: Materials.IronIngot, ratio: 1.00},
    {component: Components.AutomaticRifle, material: Materials.NickelIngot, ratio: 0.33},

    {component: Components.Drill, material: Materials.IronIngot, ratio: 6.67},
    {component: Components.Drill, material: Materials.NickelIngot, ratio: 1.00},
    {component: Components.Drill, material: Materials.SiliconWafer, ratio: 1.00},

    {component: Components.EnhancedDrill, material: Materials.IronIngot, ratio: 6.67},
    {component: Components.EnhancedDrill, material: Materials.NickelIngot, ratio: 1.00},
    {component: Components.EnhancedDrill, material: Materials.SiliconWafer, ratio: 1.67},

    {component: Components.ProficientDrill, material: Materials.IronIngot, ratio: 6.67},
    {component: Components.ProficientDrill, material: Materials.NickelIngot, ratio: 1.00},
    {component: Components.ProficientDrill, material: Materials.SiliconWafer, ratio: 1.00},
    {component: Components.ProficientDrill, material: Materials.SilverIngot, ratio: 0.67},

    {component: Components.EliteDrill, material: Materials.IronIngot, ratio: 6.67},
    {component: Components.EliteDrill, material: Materials.NickelIngot, ratio: 1.00},
    {component: Components.EliteDrill, material: Materials.SiliconWafer, ratio: 1.00},
    {component: Components.EliteDrill, material: Materials.SilverIngot, ratio: 0.67},

    {component: Components.HydrogenBottle, material: Materials.IronIngot, ratio: 26.67},
    {component: Components.HydrogenBottle, material: Materials.SiliconWafer, ratio: 3.33},
    {component: Components.HydrogenBottle, material: Materials.NickelIngot, ratio: 10.00},

    {component: Components.OxygenBottle, material: Materials.IronIngot, ratio: 26.67},
    {component: Components.OxygenBottle, material: Materials.SiliconWafer, ratio: 3.33},
    {component: Components.OxygenBottle, material: Materials.NickelIngot, ratio: 10.00},

    {component: Components.PreciseAutomaticRifle, material: Materials.IronIngot, ratio: 1.00},
    {component: Components.PreciseAutomaticRifle, material: Materials.NickelIngot, ratio: 0.33},
    {component: Components.PreciseAutomaticRifle, material: Materials.CobaltIngot, ratio: 1.67},

    {component: Components.RapidFireAutomaticRifle, material: Materials.IronIngot, ratio: 1.00},
    {component: Components.RapidFireAutomaticRifle, material: Materials.NickelIngot, ratio: 2.67},

    {component: Components.EliteAutomaticRifle, material: Materials.IronIngot, ratio: 1.00},
    {component: Components.EliteAutomaticRifle, material: Materials.NickelIngot, ratio: 0.33},
    {component: Components.EliteAutomaticRifle, material: Materials.PlatinumIngot, ratio: 1.33},
    {component: Components.EliteAutomaticRifle, material: Materials.SilverIngot, ratio: 2.00},

    {component: Components.Welder, material: Materials.IronIngot, ratio: 1.67},
    {component: Components.Welder, material: Materials.NickelIngot, ratio: 0.33},
    {component: Components.Welder, material: Materials.Gravel, ratio: 1.00},

    {component: Components.EnhancedWelder, material: Materials.IronIngot, ratio: 1.67},
    {component: Components.EnhancedWelder, material: Materials.NickelIngot, ratio: 0.33},
    {component: Components.EnhancedWelder, material: Materials.CobaltIngot, ratio: 0.07},
    {component: Components.EnhancedWelder, material: Materials.SiliconWafer, ratio: 0.67},

    {component: Components.ProficientWelder, material: Materials.IronIngot, ratio: 1.67},
    {component: Components.ProficientWelder, material: Materials.NickelIngot, ratio: 0.33},
    {component: Components.ProficientWelder, material: Materials.CobaltIngot, ratio: 0.07},
    {component: Components.ProficientWelder, material: Materials.SilverIngot, ratio: 0.67},

    {component: Components.EliteWelder, material: Materials.IronIngot, ratio: 1.67},
    {component: Components.EliteWelder, material: Materials.NickelIngot, ratio: 0.33},
    {component: Components.EliteWelder, material: Materials.CobaltIngot, ratio: 0.07},
    {component: Components.EliteWelder, material: Materials.PlatinumIngot, ratio: 0.67},
];