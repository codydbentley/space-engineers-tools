
// Definitions
import Ores from "./ores";
import Materials from "./materials";
import { Refining, YieldModifier, SpeedModifier } from "./refining";
import Components from "./components";
import Assembling from "./assembling";
import { GridTypes, Blocks } from "./blocks";
import Building from "./building";

// Services
import Format from "./Services/format";
import Ore from "./Services/ore";
import Material from "./Services/material";
import Component from "./Services/component";
import Block from "./Services/block"

export default {
    Ores,
    Materials,
    Refining,
    YieldModifier,
    SpeedModifier,
    Components,
    Assembling,
    GridTypes,
    Blocks,
    Building,
    Services: {
        Format,
        Ore,
        Material,
        Component,
        Block
    }
};