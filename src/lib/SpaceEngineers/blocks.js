const GridTypes = {
    SmallGrid: {id: "SmallGrid", name: "Small Grid"},
    LargeGrid: {id: "LargeGrid", name: "Large Grid"},
};

const BlockTypes = {
    Weapon: {id: "Weapon", name: "Weapon"},
    Tool: {id: "Tool", name: "Tool"},
    Armor: {id: "Armor", name: "Armor"},
    Cockpit: {id: "Cockpit", name: "Cockpit"},
    ConveyorCargo: {id: "ConveyorCargo", name: "Conveyor/Cargo"},
    Decorative: {id: "Decorative", name: "Decorative"},
    Power: {id: "Power", name: "Power"},
    Production: {id: "Production", name: "Production"},
    Window: {id: "Window", name: "Window"},
};

const Blocks = {
    /** SMALL GRID */
    // Light Armor
    S_LightArmorBlock: {name: "Light Armor Block", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_LightArmorSlope: {name: "Light Armor Slope", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_LightArmorCorner: {name: "Light Armor Corner", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_LightArmorInvCorner: {name: "Light Armor Inv. Corner", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_LightArmorSlope2x1x1Base: {name: "Light Armor Slope 2x1x1 Base", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_LightArmorSlope2x1x1Tip: {name: "Light Armor Slope 2x1x1 Tip", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_HalfLightArmorBlock: {name: "Half Light Armor Block", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_HalfSlopeLightArmorBlock: {name: "Half Slope Light Armor Block", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_LightArmorCorner2x1x1Base: {name: "Light Armor Corner 2x1x1 Base", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_LightArmorCorner2x1x1Tip: {name: "Light Armor Corner 2x1x1 Tip", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_LightArmorInvCorner2x1x1Base: {name: "Light Armor Inv. Corner 2x1x1 Base", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_LightArmorInvCorner2x1x1Tip: {name: "Light Armor Inv. Corner 2x1x1 Tip", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_RoundArmorSlope: {name: "Round Armor Slope", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_RoundArmorCorner: {name: "Round Armor Corner", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_RoundArmorInvCorner: {name: "Round Armor Inv. Corner", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    // Heavy Armor
    S_HeavyArmorBlock: {name: "Heavy Armor Block", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_HeavyArmorSlope: {name: "Heavy Armor Slope", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_HeavyArmorCorner: {name: "Heavy Armor Corner", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_HeavyArmorInvCorner: {name: "Heavy Armor Inv. Corner", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_HeavyArmorSlope2x1x1Base: {name: "Heavy Armor Slope 2x1x1 Base", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_HeavyArmorSlope2x1x1Tip: {name: "Heavy Armor Slope 2x1x1 Tip", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_HalfHeavyArmorBlock: {name: "Half Heavy Armor Block", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_HalfSlopeHeavyArmorBlock: {name: "Half Slope Heavy Armor Block", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_HeavyArmorCorner2x1x1Base: {name: "Heavy Armor Corner 2x1x1 Base", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_HeavyArmorCorner2x1x1Tip: {name: "Heavy Armor Corner 2x1x1 Tip", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_HeavyArmorInvCorner2x1x1Base: {name: "Heavy Armor Inv. Corner 2x1x1 Base", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_HeavyArmorInvCorner2x1x1Tip: {name: "Heavy Armor Inv. Corner 2x1x1 Tip", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_HeavyRoundArmorSlope: {name: "Heavy Armor Round Slope", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_HeavyRoundArmorCorner: {name: "Heavy Armor Round Corner", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},
    S_HeavyRoundArmorInvCorner: {name: "Heavy Armor Round Inv. Corner", blockType: BlockTypes.Armor, gridType: GridTypes.SmallGrid},

    /** LARGE GRID */
    // Light Armor
    L_LightArmorBlock: {name: "Light Armor Block", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_LightArmorSlope: {name: "Light Armor Slope", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_LightArmorCorner: {name: "Light Armor Corner", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_LightArmorInvCorner: {name: "Light Armor Inv. Corner", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_LightArmorSlope2x1x1Base: {name: "Light Armor Slope 2x1x1 Base", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_LightArmorSlope2x1x1Tip: {name: "Light Armor Slope 2x1x1 Tip", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_HalfLightArmorBlock: {name: "Half Light Armor Block", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_HalfSlopeLightArmorBlock: {name: "Half Slope Light Armor Block", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_LightArmorCorner2x1x1Base: {name: "Light Armor Corner 2x1x1 Base", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_LightArmorCorner2x1x1Tip: {name: "Light Armor Corner 2x1x1 Tip", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_LightArmorInvCorner2x1x1Base: {name: "Light Armor Inv. Corner 2x1x1 Base", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_LightArmorInvCorner2x1x1Tip: {name: "Light Armor Inv. Corner 2x1x1 Tip", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_RoundArmorSlope: {name: "Round Armor Slope", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_RoundArmorCorner: {name: "Round Armor Corner", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_RoundArmorInvCorner: {name: "Round Armor Inv. Corner", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    // Heavy Armor
    L_HeavyArmorBlock: {name: "Heavy Armor Block", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_HeavyArmorSlope: {name: "Heavy Armor Slope", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_HeavyArmorCorner: {name: "Heavy Armor Corner", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_HeavyArmorInvCorner: {name: "Heavy Armor Inv. Corner", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_HeavyArmorSlope2x1x1Base: {name: "Heavy Armor Slope 2x1x1 Base", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_HeavyArmorSlope2x1x1Tip: {name: "Heavy Armor Slope 2x1x1 Tip", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_HalfHeavyArmorBlock: {name: "Half Heavy Armor Block", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_HalfSlopeHeavyArmorBlock: {name: "Half Slope Heavy Armor Block", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_HeavyArmorCorner2x1x1Base: {name: "Heavy Armor Corner 2x1x1 Base", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_HeavyArmorCorner2x1x1Tip: {name: "Heavy Armor Corner 2x1x1 Tip", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_HeavyArmorInvCorner2x1x1Base: {name: "Heavy Armor Inv. Corner 2x1x1 Base", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_HeavyArmorInvCorner2x1x1Tip: {name: "Heavy Armor Inv. Corner 2x1x1 Tip", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_HeavyRoundArmorSlope: {name: "Heavy Armor Round Slope", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_HeavyRoundArmorCorner: {name: "Heavy Armor Round Corner", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
    L_HeavyRoundArmorInvCorner: {name: "Heavy Armor Round Inv. Corner", blockType: BlockTypes.Armor, gridType: GridTypes.LargeGrid},
};

export {
    GridTypes,
    BlockTypes,
    Blocks
};