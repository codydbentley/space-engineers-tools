export default {
    Gravel: {id: "Gravel", name: "Gravel", weight: 1, volume: 0.37},
    IronIngot: {id: "IronIngot", name: "Iron Ingot", weight: 1, volume: 0.127},
    NickelIngot: {id: "NickelIngot", name: "Nickel Ingot", weight: 1, volume: 0.112},
    SiliconWafer: {id: "SiliconWafer", name: "Silicon Wafer", weight: 1, volume: 0.429},
    MagnesiumPowder: {id: "MagnesiumPowder", name: "Magnesium Powder", weight: 1, volume: 0.575},
    CobaltIngot: {id: "CobaltIngot", name: "Cobalt Ingot", weight: 1, volume: 0.112},
    SilverIngot: {id: "SilverIngot", name: "Silver Ingot", weight: 1, volume: 0.095},
    GoldIngot: {id: "GoldIngot", name: "Gold Ingot", weight: 1, volume: 0.052},
    PlatinumIngot: {id: "PlatinumIngot", name: "Platinum Ingot", weight: 1, volume: 0.047},
    UraniumIngot: {id: "UraniumIngot", name: "Uranium Ingot", weight: 1, volume: 0.052},
};