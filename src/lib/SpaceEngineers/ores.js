export default {
    Stone: {id: "Stone", name: "Stone", weight: 1, volume: 0.37},
    Ice: {id: "Ice", name: "Ice", weight: 1, volume: 0.37},
    Iron: {id: "Iron", name: "Iron Ore", weight: 1, volume: 0.37},
    Nickel: {id: "Nickel", name: "Nickel Ore", weight: 1, volume: 0.37},
    Silicon: {id: "Silicon", name: "Silicon Ore", weight: 1, volume: 0.37},
    Magnesium: {id: "Magnesium", name: "Magnesium Ore", weight: 1, volume: 0.37},
    Cobalt: {id: "Cobalt", name: "Cobalt Ore", weight: 1, volume: 0.37},
    Silver: {id: "Silver", name: "Silver Ore", weight: 1, volume: 0.37},
    Gold: {id: "Gold", name: "Gold Ore", weight: 1, volume: 0.37},
    Platinum: {id: "Platinum", name: "Platinum Ore", weight: 1, volume: 0.37},
    Uranium: {id: "Uranium", name: "Uranium Ore", weight: 1, volume: 0.37},
};