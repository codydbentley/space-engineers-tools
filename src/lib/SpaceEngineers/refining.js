import Ores from "./ores";
import Materials from "./materials";

const Refining = [ // Ore Amount * Ratio = Material Amount
    {ore: Ores.Stone, material: Materials.Gravel, ratio: 0.014},
    {ore: Ores.Stone, material: Materials.IronIngot, ratio: 0.03},
    {ore: Ores.Stone, material: Materials.NickelIngot, ratio: 0.0024},
    {ore: Ores.Stone, material: Materials.SiliconWafer, ratio: 0.004},

    {ore: Ores.Iron, material: Materials.IronIngot, ratio: 0.7},
    {ore: Ores.Nickel, material: Materials.NickelIngot, ratio: 0.4},
    {ore: Ores.Silicon, material: Materials.SiliconWafer, ratio: 0.7},
    {ore: Ores.Magnesium, material: Materials.MagnesiumPowder, ratio: 0.007},
    {ore: Ores.Cobalt, material: Materials.CobaltIngot, ratio: 0.3},
    {ore: Ores.Silver, material: Materials.SilverIngot, ratio: 0.1},
    {ore: Ores.Gold, material: Materials.GoldIngot, ratio: 0.01},
    {ore: Ores.Platinum, material: Materials.PlatinumIngot, ratio: 0.005},
    {ore: Ores.Uranium, material: Materials.UraniumIngot, ratio: 0.01},
];

const YieldModifier = {
    0: {modifier: 1},
    1: {modifier: 1.19},
    2: {modifier: 1.41},
    3: {modifier: 1.68},
    4: {modifier: 2},
};

const SpeedModifier = {
    0: {modifier: 1},
    1: {modifier: 2},
    2: {modifier: 3},
    3: {modifier: 4},
    4: {modifier: 5},
};

export {
    Refining,
    YieldModifier,
    SpeedModifier
}