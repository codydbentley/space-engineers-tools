import {Blocks} from "./blocks";
import Components from "./components";

export default [
    /** SMALL GRID */
    // Light armor blocks are all the same
    {block: Blocks.S_LightArmorBlock, component: Components.SteelPlate, ratio: 1},
    {block: Blocks.S_LightArmorSlope, component: Components.SteelPlate, ratio: 1},
    {block: Blocks.S_LightArmorCorner, component: Components.SteelPlate, ratio: 1},
    {block: Blocks.S_LightArmorInvCorner, component: Components.SteelPlate, ratio: 1},
    {block: Blocks.S_LightArmorSlope2x1x1Base, component: Components.SteelPlate, ratio: 1},
    {block: Blocks.S_LightArmorSlope2x1x1Tip, component: Components.SteelPlate, ratio: 1},
    {block: Blocks.S_HalfLightArmorBlock, component: Components.SteelPlate, ratio: 1},
    {block: Blocks.S_HalfSlopeLightArmorBlock, component: Components.SteelPlate, ratio: 1},
    {block: Blocks.S_LightArmorCorner2x1x1Base, component: Components.SteelPlate, ratio: 1},
    {block: Blocks.S_LightArmorCorner2x1x1Tip, component: Components.SteelPlate, ratio: 1},
    {block: Blocks.S_LightArmorInvCorner2x1x1Base, component: Components.SteelPlate, ratio: 1},
    {block: Blocks.S_LightArmorInvCorner2x1x1Tip, component: Components.SteelPlate, ratio: 1},
    {block: Blocks.S_RoundArmorSlope, component: Components.SteelPlate, ratio: 1},
    {block: Blocks.S_RoundArmorCorner, component: Components.SteelPlate, ratio: 1},
    {block: Blocks.S_RoundArmorInvCorner, component: Components.SteelPlate, ratio: 1},
    // Heavy Armor
    {block: Blocks.S_HeavyArmorBlock, component: Components.MetalGrid, ratio: 2},
    {block: Blocks.S_HeavyArmorBlock, component: Components.SteelPlate, ratio: 5},

    {block: Blocks.S_HeavyArmorSlope, component: Components.MetalGrid, ratio: 1},
    {block: Blocks.S_HeavyArmorSlope, component: Components.SteelPlate, ratio: 3},

    {block: Blocks.S_HeavyArmorCorner, component: Components.MetalGrid, ratio: 1},
    {block: Blocks.S_HeavyArmorCorner, component: Components.SteelPlate, ratio: 2},

    {block: Blocks.S_HeavyArmorInvCorner, component: Components.MetalGrid, ratio: 1},
    {block: Blocks.S_HeavyArmorInvCorner, component: Components.SteelPlate, ratio: 4},

    {block: Blocks.S_HeavyArmorSlope2x1x1Base, component: Components.MetalGrid, ratio: 1},
    {block: Blocks.S_HeavyArmorSlope2x1x1Base, component: Components.SteelPlate, ratio: 4},

    {block: Blocks.S_HeavyArmorSlope2x1x1Tip, component: Components.MetalGrid, ratio: 1},
    {block: Blocks.S_HeavyArmorSlope2x1x1Tip, component: Components.SteelPlate, ratio: 2},

    {block: Blocks.S_HalfHeavyArmorBlock, component: Components.MetalGrid, ratio: 1},
    {block: Blocks.S_HalfHeavyArmorBlock, component: Components.SteelPlate, ratio: 3},

    {block: Blocks.S_HalfSlopeHeavyArmorBlock, component: Components.MetalGrid, ratio: 1},
    {block: Blocks.S_HalfSlopeHeavyArmorBlock, component: Components.SteelPlate, ratio: 2},

    {block: Blocks.S_HeavyArmorCorner2x1x1Base, component: Components.MetalGrid, ratio: 1},
    {block: Blocks.S_HeavyArmorCorner2x1x1Base, component: Components.SteelPlate, ratio: 3},

    {block: Blocks.S_HeavyArmorCorner2x1x1Tip, component: Components.MetalGrid, ratio: 1},
    {block: Blocks.S_HeavyArmorCorner2x1x1Tip, component: Components.SteelPlate, ratio: 2},

    {block: Blocks.S_HeavyArmorInvCorner2x1x1Base, component: Components.MetalGrid, ratio: 1},
    {block: Blocks.S_HeavyArmorInvCorner2x1x1Base, component: Components.SteelPlate, ratio: 5},

    {block: Blocks.S_HeavyArmorInvCorner2x1x1Tip, component: Components.MetalGrid, ratio: 1},
    {block: Blocks.S_HeavyArmorInvCorner2x1x1Tip, component: Components.SteelPlate, ratio: 5},

    {block: Blocks.S_HeavyRoundArmorSlope, component: Components.MetalGrid, ratio: 1},
    {block: Blocks.S_HeavyRoundArmorSlope, component: Components.SteelPlate, ratio: 4},

    {block: Blocks.S_HeavyRoundArmorCorner, component: Components.MetalGrid, ratio: 1},
    {block: Blocks.S_HeavyRoundArmorCorner, component: Components.SteelPlate, ratio: 4},

    {block: Blocks.S_HeavyRoundArmorInvCorner, component: Components.MetalGrid, ratio: 1},
    {block: Blocks.S_HeavyRoundArmorInvCorner, component: Components.SteelPlate, ratio: 5},

    /** LARGE GRID */
    // Light armor
    {block: Blocks.L_LightArmorBlock, component: Components.SteelPlate, ratio: 25},

    {block: Blocks.L_LightArmorSlope, component: Components.SteelPlate, ratio: 13},

    {block: Blocks.L_LightArmorCorner, component: Components.SteelPlate, ratio: 4},

    {block: Blocks.L_LightArmorInvCorner, component: Components.SteelPlate, ratio: 21},

    {block: Blocks.L_LightArmorSlope2x1x1Base, component: Components.SteelPlate, ratio: 19},

    {block: Blocks.L_LightArmorSlope2x1x1Tip, component: Components.SteelPlate, ratio: 7},

    {block: Blocks.L_HalfLightArmorBlock, component: Components.SteelPlate, ratio: 12},

    {block: Blocks.L_HalfSlopeLightArmorBlock, component: Components.SteelPlate, ratio: 7},

    {block: Blocks.L_LightArmorCorner2x1x1Base, component: Components.SteelPlate, ratio: 10},

    {block: Blocks.L_LightArmorCorner2x1x1Tip, component: Components.SteelPlate, ratio: 4},

    {block: Blocks.L_LightArmorInvCorner2x1x1Base, component: Components.SteelPlate, ratio: 22},

    {block: Blocks.L_LightArmorInvCorner2x1x1Tip, component: Components.SteelPlate, ratio: 16},

    {block: Blocks.L_RoundArmorSlope, component: Components.SteelPlate, ratio: 13},

    {block: Blocks.L_RoundArmorCorner, component: Components.SteelPlate, ratio: 4},

    {block: Blocks.L_RoundArmorInvCorner, component: Components.SteelPlate, ratio: 21},

    // Heavy Armor
    {block: Blocks.L_HeavyArmorBlock, component: Components.MetalGrid, ratio: 50},
    {block: Blocks.L_HeavyArmorBlock, component: Components.SteelPlate, ratio: 100},

    {block: Blocks.L_HeavyArmorSlope, component: Components.MetalGrid, ratio: 25},
    {block: Blocks.L_HeavyArmorSlope, component: Components.SteelPlate, ratio: 75},

    {block: Blocks.L_HeavyArmorCorner, component: Components.MetalGrid, ratio: 10},
    {block: Blocks.L_HeavyArmorCorner, component: Components.SteelPlate, ratio: 25},

    {block: Blocks.L_HeavyArmorInvCorner, component: Components.MetalGrid, ratio: 50},
    {block: Blocks.L_HeavyArmorInvCorner, component: Components.SteelPlate, ratio: 125},

    {block: Blocks.L_HeavyArmorSlope2x1x1Base, component: Components.MetalGrid, ratio: 45},
    {block: Blocks.L_HeavyArmorSlope2x1x1Base, component: Components.SteelPlate, ratio: 112},

    {block: Blocks.L_HeavyArmorSlope2x1x1Tip, component: Components.MetalGrid, ratio: 6},
    {block: Blocks.L_HeavyArmorSlope2x1x1Tip, component: Components.SteelPlate, ratio: 35},

    {block: Blocks.L_HalfHeavyArmorBlock, component: Components.MetalGrid, ratio: 25},
    {block: Blocks.L_HalfHeavyArmorBlock, component: Components.SteelPlate, ratio: 75},

    {block: Blocks.L_HalfSlopeHeavyArmorBlock, component: Components.MetalGrid, ratio: 15},
    {block: Blocks.L_HalfSlopeHeavyArmorBlock, component: Components.SteelPlate, ratio: 45},

    {block: Blocks.L_HeavyArmorCorner2x1x1Base, component: Components.MetalGrid, ratio: 15},
    {block: Blocks.L_HeavyArmorCorner2x1x1Base, component: Components.SteelPlate, ratio: 55},

    {block: Blocks.L_HeavyArmorCorner2x1x1Tip, component: Components.MetalGrid, ratio: 6},
    {block: Blocks.L_HeavyArmorCorner2x1x1Tip, component: Components.SteelPlate, ratio: 19},

    {block: Blocks.L_HeavyArmorInvCorner2x1x1Base, component: Components.MetalGrid, ratio: 45},
    {block: Blocks.L_HeavyArmorInvCorner2x1x1Base, component: Components.SteelPlate, ratio: 133},

    {block: Blocks.L_HeavyArmorInvCorner2x1x1Tip, component: Components.MetalGrid, ratio: 25},
    {block: Blocks.L_HeavyArmorInvCorner2x1x1Tip, component: Components.SteelPlate, ratio: 94},

    {block: Blocks.L_HeavyRoundArmorSlope, component: Components.MetalGrid, ratio: 50},
    {block: Blocks.L_HeavyRoundArmorSlope, component: Components.SteelPlate, ratio: 130},

    {block: Blocks.L_HeavyRoundArmorCorner, component: Components.MetalGrid, ratio: 40},
    {block: Blocks.L_HeavyRoundArmorCorner, component: Components.SteelPlate, ratio: 125},

    {block: Blocks.L_HeavyRoundArmorInvCorner, component: Components.MetalGrid, ratio: 50},
    {block: Blocks.L_HeavyRoundArmorInvCorner, component: Components.SteelPlate, ratio: 140},
];