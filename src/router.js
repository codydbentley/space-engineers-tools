import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from './views/Home.vue'

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/Ores',
        name: 'Ores',
        component: () => import(/* webpackChunkName: "Ores" */ './views/Ores.vue'),
    },
    {
        path: '/Ores/:ore',
        name: 'Ore',
        component: () => import(/* webpackChunkName: "Ore" */ './views/Ore.vue')
    },
    {
        path: '/Materials',
        name: 'Materials',
        component: () => import(/* webpackChunkName: "Materials" */ './views/Materials.vue'),
    },
    {
        path: '/Materials/:material',
        name: 'Material',
        component: () => import(/* webpackChunkName: "Material" */ './views/Material.vue')
    },
    {
        path: '/Components',
        name: 'CraftComponents',
        component: () => import(/* webpackChunkName: "CraftComponents" */ './views/CraftComponents.vue'),
    },
    {
        path: '/Components/:component',
        name: 'CraftComponent',
        component: () => import(/* webpackChunkName: "CraftComponent" */ './views/CraftComponent.vue')
    },
    {
        path: '/Blocks',
        name: 'Blocks',
        component: () => import(/* webpackChunkName: "Blocks" */ './views/Blocks.vue'),
    },
    {
        path: '/Blocks/:block',
        name: 'Block',
        component: () => import(/* webpackChunkName: "Block" */ './views/Block.vue')
    },
    {
        path: '/Apps/BuildPlanner',
        name: 'BuildPlanner',
        component: () => import(/* webpackChunkName: "BuildPlanner" */ './views/BuildPlanner.vue')
    },
    {
        path: '*',
        name: 'NotFound',
        component: () => import(/* webpackChunkName: "NotFound" */ './views/NotFound.vue')
    },
];

const router = new VueRouter({
    routes
});

export default router;
