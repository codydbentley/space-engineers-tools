import SpaceEngineers from '../lib/SpaceEngineers'

export default {
    data() {
        return {
            SpaceEngineers
        }
    },
    methods: {
        weight(amt) {
            return this.SpaceEngineers.Services.Format.Kilograms(amt);
        },
        volume(amt) {
            return this.SpaceEngineers.Services.Format.Liters(amt);
        }
    }
};