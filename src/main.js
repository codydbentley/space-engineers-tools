import Vue from 'vue'
import App from './App.vue'
import router from './router.js'

import Bootstrap from 'bootstrap'
import Style from './styles/style.scss'

import SpaceEngineersMixin from '@/mixins/SpaceEngineers.js';
import SEToolsMixin from '@/mixins/SETools.js';

Vue.config.productionTip = false;
Vue.mixin(SpaceEngineersMixin);
Vue.mixin(SEToolsMixin);

new Vue({
    router,
    render: h => h(App)
}).$mount('#app');
